class persona {
    constructor(nombre, edad, DNI, peso, altura, sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.DNI = DNI;
        this.peso = peso;
        this.altura = altura;
        this.sexo = sexo; 
    }

    calcularlMC(){
        let pesoIde = (this.peso) / (this.altura * this.altura);

        if(pesoIde < 20){
            return -1;
        }
        else if(pesoIde <4 || pesoIde >20){
            return 0;
        }
        else{
            return 1; 
        }
    }

    esMayorDeEdad(){

        if(this.edad > 18){
            return true;
            
        }
        else{
            return false;
        }
            
    }

    comprobarSexo(){
        if(this.sexo == "M"){
            return "Es mujer";
        }
        else{
            return "Es hombre";
        }
    }
}