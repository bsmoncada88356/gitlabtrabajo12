class Cuenta{
    constructor(titular, cantidad){
        this.titular = titular;
        this.cantidad = cantidad;
        
    }

    mostrar(){
        console.log(this.titular);
        console.log(this.cantidad);
    }

    ingresar_cantidad(){

        
        let ingCant = Number(prompt ("Ingrese una cantidad"));

        
        if(ingCant < 0){
            console.log("No se puede ingresar esa cantidad")
        }else{
            console.log("Se ha ingresado el monto de  " + ' ' + ingCant);
            let suma = ingCant + this.cantidad;
            this.cantidad = suma; 
            console.log("Su saldo es: " + " " + suma)

            
        }

    }

    retirar_dinero(){

    

        let retCant = prompt("Ingrese la cantidad que desea retirar: ");

        
        console.log("El monto retirado es: " + " " + retCant);   
        console.log("El saldo total es: " + " " + this.cantidad);

        var rest = this.cantidad - retCant;
        this.cantidad = rest;

        console.log("Su saldo total es: " + " " + rest);
    }

}